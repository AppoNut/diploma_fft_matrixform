#include <complex.h>

#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#pragma pack(2)

#include "structs.h"


int main( int argc, char **argv ) {

	printf("%d %d %d %d \n", sizeof(BitmapCoreHeader), sizeof(BmpInfoHeader), sizeof(BmpHeaderVer4), sizeof(BmpHeaderVer5));
	if(argc < 3){
		printf("too few arguments");
		return EXIT_FAILURE;
	}

	BmpHeader *header = (BmpHeader*)malloc(sizeof(BmpHeader));
	BmpInfoHeader *info = (BmpInfoHeader*)malloc(sizeof(BmpInfoHeader));
	Picture* picture;

	picture = (Picture*)malloc(sizeof(Picture));

	if (loadPicture(argv[1], header, info, picture) == EXIT_FAILURE){
		return EXIT_FAILURE;
	}
	else{
		printf("\n");
		uint32_t myPictureSize = picture->width * picture->height;

		uint32_t myPictureDimension = checkDimension(myPictureSize, 1, 4);

		printf("pic dimen - %d\n", myPictureDimension);
		printf("pic size - %d\n", myPictureSize);

		printf("width - %d\n", checkDimension(picture->width, 1, 4));
		printf("height - %d\n", checkDimension(picture->height, 1, 4));

		printf("Picture with zeros\n");

		Picture* pictureWithZeros = (Picture*)malloc(sizeof(Picture));
		pictureWithZeros->width = checkDimension(picture->width, 1, 4);
		pictureWithZeros->height = checkDimension(picture->height, 1, 4);
		pictureWithZeros->pixelArray = zeroFill(picture, pictureWithZeros->width, pictureWithZeros->height);

		Picture * restoredPicture = (Picture*)malloc(sizeof(Picture));
		restoredPicture->height = picture->height;
		restoredPicture->width = picture->width;
		restoredPicture->pixelArray = (Rgb**)malloc(sizeof(Rgb*) * restoredPicture->height);
		for(int i = 0; i < restoredPicture->height; i++ ) {
			restoredPicture->pixelArray[i] = (Rgb*)malloc(sizeof(Rgb) * restoredPicture->width);
		}

		double complex ** redSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);
		redSpectre = makeSpectre(pictureWithZeros->pixelArray, pictureWithZeros->width, pictureWithZeros->height, 0, 0, 0);

		printf("-- RED SPECTRE --\n");
		for(int j = 0; j < pictureWithZeros->height; j++){
			for(int i = 0; i < pictureWithZeros->width; i++){
				printf("   [%d][%d]: Real: %f , Imag: %f ", j, i, creal(redSpectre[j][i]), cimag(redSpectre[j][i]));
			}
			printf("\n");
		}

		double complex ** greenSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);
		greenSpectre = makeSpectre(pictureWithZeros->pixelArray, pictureWithZeros->width, pictureWithZeros->height, 0, 0, 1);

		double complex ** blueSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);
		blueSpectre = makeSpectre(pictureWithZeros->pixelArray, pictureWithZeros->width, pictureWithZeros->height, 0, 0, 2);

		double complex ** reverseRedSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);
		double complex ** reverseGreenSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);
		double complex ** reverseBlueSpectre = (double complex**)malloc(sizeof(double complex*) * pictureWithZeros->height);

		reverseRedSpectre = reverseSpectre(redSpectre, pictureWithZeros->width, pictureWithZeros->height, 0, 0);
		reverseGreenSpectre = reverseSpectre(greenSpectre, pictureWithZeros->width, pictureWithZeros->height, 0, 0);
		reverseBlueSpectre = reverseSpectre(blueSpectre, pictureWithZeros->width, pictureWithZeros->height, 0, 0);

		printf("-- REVERSE RED SPECTRE --\n");
		for(int j = 0; j < pictureWithZeros->height; j++){
			for(int i = 0; i < pictureWithZeros->width; i++){
				printf("   [%d][%d]: Real: %f , Imag: %f ", j, i, creal(reverseRedSpectre[j][i]), cimag(reverseRedSpectre[j][i]));
			}
			printf("\n");
		}

		writeSpectreInPicture(reverseRedSpectre, 0, restoredPicture);
		writeSpectreInPicture(reverseGreenSpectre, 1, restoredPicture);
		writeSpectreInPicture(reverseBlueSpectre, 2, restoredPicture);


		printf("-- RESTORED --\n");
		for(int j = 0; j < restoredPicture->height; j++){
			for(int i = 0; i < restoredPicture->width; i++){
				printf("   [%d][%d]: %d ", j, i, restoredPicture->pixelArray[j][i].red);
			}
			printf("\n");
		}

		if (writePictureToFile(argv[2], header, info, restoredPicture) == EXIT_FAILURE){
			return EXIT_FAILURE;
		}

		/*printf("FFT\n");
		uint32_t start_time = clock();
		uint32_t end_time = clock();
		uint32_t result_time = end_time - start_time;
		printf("SPECTRUM %f\n", (float)result_time/CLOCKS_PER_SEC);*/

		return 0;
	}
}
