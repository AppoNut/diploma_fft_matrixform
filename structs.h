#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define PI 3.14159265359

typedef struct{
    char signature[2];
    uint32_t fileSize;
    uint32_t reserved;
    uint32_t offset;
} __attribute__ ((__packed__)) BmpHeader;

typedef struct{
	uint32_t headerSize;
	uint16_t width;
	uint16_t height;
	uint16_t planeCount;
	uint16_t bitDepth;
} BitmapCoreHeader;

typedef struct{
	uint32_t headerSize;
	uint32_t width;
	uint32_t height;
	uint16_t planeCount;
	uint16_t bitDepth;
    uint32_t compression;
    uint32_t compressedImageSize;
    uint32_t horizontalResolution;
    uint32_t verticalResolution;
    uint32_t numColors;
    uint32_t importantColors;
} __attribute__ ((__packed__)) BmpInfoHeader;

typedef struct{
	BmpInfoHeader info;
	uint32_t redMask;
	uint32_t greenMask;
	uint32_t blueMask;
	uint32_t alphaMask;
	uint32_t csType;
	uint32_t endPoints[9];
	uint32_t gammaRed;
	uint32_t gammaGreen;
	uint32_t gammaBlue;
} BmpHeaderVer4;

typedef struct{
	BmpHeaderVer4 bmpHeaderVer4;
	uint32_t bIntent;
	uint32_t bProfileData;
	uint32_t bProfileSize;
	uint32_t bReserved;
} BmpHeaderVer5;

typedef struct
{
    uint8_t blue;
    uint8_t green;
    uint8_t red;
} Rgb;

typedef struct{
	int width;
	int height;
	Rgb **pixelArray;
} Picture;

typedef struct{
	double complex ** redSpectre;
	double complex ** greenSpectre;
	double complex ** blueSpectre;
} PictureSpectre;

int loadPicture(const char * filename, BmpHeader * bmpHeader, BmpInfoHeader * bmpInfoHeader, Picture * picture);
uint32_t checkDimension(uint32_t widthWithHeight, uint32_t valueWithGrade, uint32_t base);
Rgb ** zeroFill(Picture * picture, uint32_t widthDifferences, uint32_t heightDiffences);
double complex ** makeSpectre(Rgb ** pixelArray, uint32_t widthSize, uint32_t heightSize, uint32_t widthOffset, uint32_t heightOffset, uint32_t colorType);
int writePictureToFile(const char * filename, BmpHeader * bmpHeader, BmpInfoHeader * bmpInfoHeader, Picture * picture);
Picture * writeSpectreInPicture(double complex ** spectre, uint32_t colorType, Picture * picture);


double complex ** reverseFastFurieTransform(double complex ** pixelArray, uint32_t widthOffset, uint32_t heightOffset);
double complex ** reverseButterfly(double complex ** quarter1, double complex ** quarter2, double complex ** quarter3, double complex ** quarter4, uint32_t height, uint32_t width);
double complex ** reverseSpectre(double complex ** pixelArray, uint32_t widthSize, uint32_t heightSize, uint32_t widthOffset, uint32_t heightOffset);
