#include <complex.h>
#include <math.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "structs.h"

uint32_t checkDimension(uint32_t widthWithHeight, uint32_t valueWithGrade, uint32_t base){
	return (valueWithGrade < widthWithHeight)? checkDimension(widthWithHeight, valueWithGrade * base, base) : valueWithGrade;
}


double complex ** FastFurieTransform(Rgb ** pixelArray, uint32_t widthOffset, uint32_t heightOffset, uint32_t colorType){
	/* 0 1 2 3 */
	/* 4 5 6 7 */
	//double complex * spectre = (double complex *)malloc(sizeof(double complex) * 4);
	//double complex * row = (double complex *)malloc(sizeof(double complex) * 4);

	double complex **row = (double complex**)malloc(sizeof(double complex) * 2);
	for(int i = 0; i < 2; i++ ) {
		row[i] = (double complex *) malloc( sizeof(double complex) * 2);
	}

	double complex **spectre = (double complex**)malloc(sizeof(double complex *) * 2);
	for(int i = 0; i < 2; i++ ) {
		spectre[i] = (double complex *) malloc( sizeof(double complex) * 2);
	}

	if(colorType == 0){
		row[0][0] = (double complex)(pixelArray[heightOffset][widthOffset].red + pixelArray[heightOffset][widthOffset+1].red)/2;
		row[0][1] = (double complex)(pixelArray[heightOffset][widthOffset].red - pixelArray[heightOffset][widthOffset+1].red)/2;
		row[1][0] = (double complex)(pixelArray[heightOffset+1][widthOffset].red + pixelArray[heightOffset+1][widthOffset+1].red)/2;
		row[1][1] = (double complex)(pixelArray[heightOffset+1][widthOffset].red - pixelArray[heightOffset+1][widthOffset+1].red)/2;

		spectre[0][0] = (double complex)(row[0][0] + row[1][0])/2;
		spectre[0][1] = (double complex)(row[0][1] + row[1][1])/2;
		spectre[1][0] = (double complex)(row[0][0] - row[1][0])/2;
		spectre[1][1] = (double complex)(row[0][1] - row[1][1])/2;
	}

	if(colorType == 1){
		row[0][0] = (double complex)(pixelArray[heightOffset][widthOffset].green + pixelArray[heightOffset][widthOffset+1].green)/2;
		row[0][1] = (double complex)(pixelArray[heightOffset][widthOffset].green - pixelArray[heightOffset][widthOffset+1].green)/2;
		row[1][0] = (double complex)(pixelArray[heightOffset+1][widthOffset].green + pixelArray[heightOffset+1][widthOffset+1].green)/2;
		row[1][1] = (double complex)(pixelArray[heightOffset+1][widthOffset].green - pixelArray[heightOffset+1][widthOffset+1].green)/2;

		spectre[0][0] = (double complex)(row[0][0] + row[1][0])/2;
		spectre[0][1] = (double complex)(row[0][1] + row[1][1])/2;
		spectre[1][0] = (double complex)(row[0][0] - row[1][0])/2;
		spectre[1][1] = (double complex)(row[0][1] - row[1][1])/2;
	}

	if(colorType == 2){
		row[0][0] = (double complex)(pixelArray[heightOffset][widthOffset].blue + pixelArray[heightOffset][widthOffset+1].blue)/2;
		row[0][1] = (double complex)(pixelArray[heightOffset][widthOffset].blue - pixelArray[heightOffset][widthOffset+1].blue)/2;
		row[1][0] = (double complex)(pixelArray[heightOffset+1][widthOffset].blue + pixelArray[heightOffset+1][widthOffset+1].blue)/2;
		row[1][1] = (double complex)(pixelArray[heightOffset+1][widthOffset].blue - pixelArray[heightOffset+1][widthOffset+1].blue)/2;

		spectre[0][0] = (double complex)(row[0][0] + row[1][0])/2;
		spectre[0][1] = (double complex)(row[0][1] + row[1][1])/2;
		spectre[1][0] = (double complex)(row[0][0] - row[1][0])/2;
		spectre[1][1] = (double complex)(row[0][1] - row[1][1])/2;
	}

	/*for(int j = 0; j < 2; j++){
		for(int i = 0; i < 2; i++){
			printf("[%d][%d] Real: %f , Imag: %f\n", j, i, creal(spectre[j][i]), cimag(spectre[j][i]));
		}
	}

	printf("\n");*/
	return spectre;
}

double complex ** butterfly(double complex ** quarter1, double complex ** quarter2, double complex ** quarter3, double complex ** quarter4, uint32_t height, uint32_t width){
	//memory
	double complex **leftVerticalMatrix = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		leftVerticalMatrix[i] = (double complex *) malloc( sizeof(double complex) * width/2);
	}

	double complex **rightVerticalMatrix = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		rightVerticalMatrix[i] = (double complex *) malloc( sizeof(double complex) * width);
	}

	double complex **result = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		result[i] = (double complex *) malloc( sizeof(double complex) * width);
	}

//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width/2; i++){
			if (j < height/2){
				leftVerticalMatrix[j][i] = ( quarter1[j][i] + quarter3[j][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
			}
			else
				leftVerticalMatrix[j][i] = ( quarter1[j-height/2][i] + quarter3[j-height/2][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
		}
	}
//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width/2; i++){
			if (j < height/2){
				rightVerticalMatrix[j][i] = ( quarter2[j][i] + quarter4[j][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
			}
			else
				rightVerticalMatrix[j][i] = ( quarter2[j-height/2][i] + quarter4[j-height/2][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
		}
	}
//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width; i++){
			if (i < width/2){
				result[j][i] = ( leftVerticalMatrix[j][i] + rightVerticalMatrix[j][i] * cexp( -I * 2 * PI * (i+j*width)  / (height*width) )  )/2;
			}
			else
				result[j][i] = ( leftVerticalMatrix[j][i-width/2] + rightVerticalMatrix[j][i-width/2] * cexp( -I * 2 * PI * (i+j*width)  / (height*width) )  )/2;
		}
	}
//--------------------------------------------------------------------

	for(int j = 0; j < height; j++){
		for(int i = 0; i < width; i++){
		/*for(int i = 0; i < width/2; i++){
			printf("--- LEFT MATRIX RESULT --- [%d][%d] Real: %f , Imag: %f\n", j, i, creal(leftVerticalMatrix[j][i]), cimag(leftVerticalMatrix[j][i]));
			printf("--- RIGHT MATRIX RESULT --- [%d][%d] Real: %f , Imag: %f\n", j, i, creal(rightVerticalMatrix[j][i]), cimag(rightVerticalMatrix[j][i]));
			*/
			//printf("--- MATRIX RESULT --- [%d][%d] Real: %f , Imag: %f\n", j, i, creal(result[j][i]), cimag(result[j][i]));
		}
	}

	return result;
}

double complex ** makeSpectre(Rgb ** pixelArray, uint32_t widthSize, uint32_t heightSize, uint32_t widthOffset, uint32_t heightOffset, uint32_t colorType){
	double complex ** quarter1;
	double complex ** quarter2;
	double complex ** quarter3;
	double complex ** quarter4;

	if (widthSize == 2 && heightSize == 2){
		//printf("Inside matrix\n");
		return (double complex **)FastFurieTransform(pixelArray, widthOffset, heightOffset, colorType);
	}
	else{
		//������� �������� ������
		quarter1 = makeSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset, heightOffset, colorType);
		quarter2 = makeSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset + widthSize/2, heightOffset, colorType);

		//������ ��������
		quarter3 = makeSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset, heightOffset + heightSize/2, colorType);
		quarter4 = makeSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset + widthSize/2, heightOffset + heightSize/2, colorType);
	}
	return (double complex **)butterfly( quarter1, quarter2, quarter3, quarter4, heightSize, widthSize );
}

Rgb ** zeroFill(Picture * picture, uint32_t widthDifferences, uint32_t heightDiffences){

	Rgb **pictureWithZeros;
	pictureWithZeros = (Rgb**)malloc(sizeof(Rgb*)*heightDiffences);

	for(int i = 0; i < heightDiffences; i++){
		pictureWithZeros[i] = (Rgb*)malloc(sizeof(Rgb)*widthDifferences);
	}

	for(int i = 0; i < heightDiffences; i++){
		for(int j = 0; j < widthDifferences; j++){
			//printf( "Pixel [%d %d]: %3d %3d %3d\n", i, j, picture->pixelArray[i][j].red, picture->pixelArray[i][j].green, picture->pixelArray[i][j].blue);
			//fflush(stdout);
			if(i >= picture->height || j >= picture->width){
				pictureWithZeros[i][j].red = 0;
				pictureWithZeros[i][j].blue = 0;
				pictureWithZeros[i][j].green = 0;
			}
			else{
				pictureWithZeros[i][j] = picture->pixelArray[i][j];
			}
		}
	}

	for(int i = 0; i < heightDiffences; i++){
		for(int j = 0; j < widthDifferences; j++){
			//printf("%d %d %d\n", pictureWithZeros[i][j].red, pictureWithZeros[i][j].green, pictureWithZeros[i][j].blue);
			printf("[%d][%d]: %d ", i, j, pictureWithZeros[i][j].red);
			fflush(stdout);
		}
		printf("\n");
	}
	return pictureWithZeros;
}

int loadPicture(const char * filename, BmpHeader * bmpHeader, BmpInfoHeader * bmpInfoHeader, Picture * picture){
	FILE * inFile;

	printf( "Opening file %s for reading.\n", filename );

	inFile = fopen( filename, "rb" );
	if( !inFile ) {
		printf( "Error opening file %s.\n", filename );
	    return EXIT_FAILURE;
	}

	if( fread(bmpHeader, 1, sizeof(BmpHeader), inFile) != sizeof(BmpHeader) ) {
		printf( "Error reading bmp header.\n" );
	    return EXIT_FAILURE;
	}

	if( fread(bmpInfoHeader, 1, sizeof(BmpInfoHeader), inFile) != sizeof(BmpInfoHeader) ) {
		printf( "Error reading image info header.\n" );
	    return EXIT_FAILURE;
	}

	/*Rgb *palette;

	if(bmpInfoHeader->numColors > 0){
		printf("Reading palette.\n");
		palette = (Rgb*)malloc(sizeof(Rgb) * bmpInfoHeader->numColors);
		if (fread(palette, sizeof(Rgb), bmpInfoHeader->numColors, inFile) != (bmpInfoHeader->numColors * sizeof(Rgb))){
			printf( "Error reading image palette.\n" );
			return EXIT_FAILURE;
		}
	}*/
	picture->width = bmpInfoHeader->width;
	picture->height = bmpInfoHeader->height;

	//�������� ������ ��� ������ ��������
	picture->pixelArray =(Rgb**)malloc(sizeof(Rgb*) * bmpInfoHeader->height);
	for(int i = 0; i < bmpInfoHeader->height; i++ ) {
		picture->pixelArray[i] = (Rgb*) malloc( sizeof(Rgb) * bmpInfoHeader->width);
	}

	printf("W: %d H:%d\n", bmpInfoHeader->width, bmpInfoHeader->height);
	fflush(stdout);

	//�������� ������ ��� ��������� ����� ��������
	Rgb **pixel = (Rgb**) malloc( sizeof(Rgb*) * bmpInfoHeader->height);
	for(int i = 0; i < bmpInfoHeader->height; i++ ) {
		pixel[i] = (Rgb*) malloc( sizeof(Rgb) * bmpInfoHeader->width);
	}


	int read;
	for(int j = 0; j < bmpInfoHeader->height; j++ ) {
		read = 0;
		for(int i = 0; i < bmpInfoHeader->width; i++ ) {
			if( fread(&pixel[j][i], 1, sizeof(Rgb), inFile) != sizeof(Rgb) ) {
				printf( "Error reading [%d][%d] pixel!\n", j, i );
				return EXIT_FAILURE;
			}
			read += sizeof(Rgb);
			picture->pixelArray[j][i] = pixel[j][i];
		}
	}
	printf("Reading done\n");
	fclose(inFile);

	return EXIT_SUCCESS;
}

int writePictureToFile(const char * filename, BmpHeader * bmpHeader, BmpInfoHeader * bmpInfoHeader, Picture * picture){
	FILE * outFile = fopen( filename, "wb" );
	printf( "Opening file %s for writing.\n", filename );

	if( !outFile ) {
		printf( "Error opening output file %s.\n", filename );
		return EXIT_FAILURE;
	}

	fwrite(bmpHeader, 1, sizeof(BmpHeader), outFile);
	fwrite(bmpInfoHeader, 1, sizeof(BmpInfoHeader), outFile);

	for(int j = 0; j < bmpInfoHeader->height; j++){
		for(int i = 0; i < bmpInfoHeader->width; i++){
			//fwrite(&picture->pixelArray[j][i], sizeof(Rgb), 1, outFile);
			fwrite(&picture->pixelArray[j][i].red, sizeof(Rgb), 1, outFile);
			fwrite(&picture->pixelArray[j][i].green, sizeof(Rgb), 1, outFile);
			fwrite(&picture->pixelArray[j][i].blue, sizeof(Rgb), 1, outFile);
		}
	}

	printf("Writing done.\n");
	fclose(outFile);
	return EXIT_SUCCESS;
}

Picture * writeSpectreInPicture(double complex ** spectre, uint32_t colorType, Picture * picture){
	for(int j = 0; j < picture->height; j++){
		for(int i = 0; i < picture->width; i++){
			if (colorType == 0)
				picture->pixelArray[j][i].red = cabs(spectre[j][i]); //* 20 ;
			if (colorType == 1)
				picture->pixelArray[j][i].green = cabs(spectre[j][i]); //* 10 ;
			if (colorType == 2)
				picture->pixelArray[j][i].blue = cabs(spectre[j][i]); //* 10 ;
		}
	}
	return picture;
}

//****************************************************************************************************************************************************************
double complex ** reverseFastFurieTransform(double complex ** pixelArray, uint32_t widthOffset, uint32_t heightOffset){

	double complex **row = (double complex**)malloc(sizeof(double complex) * 2);
	for(int i = 0; i < 2; i++ ) {
		row[i] = (double complex *) malloc( sizeof(double complex) * 2);
	}

	double complex **spectre = (double complex**)malloc(sizeof(double complex *) * 2);
	for(int i = 0; i < 2; i++ ) {
		spectre[i] = (double complex *) malloc( sizeof(double complex) * 2);
	}

	row[0][0] = (double complex)(pixelArray[heightOffset][widthOffset] + pixelArray[heightOffset][widthOffset+1])/2;
	row[0][1] = (double complex)(pixelArray[heightOffset][widthOffset] - pixelArray[heightOffset][widthOffset+1])/2;
	row[1][0] = (double complex)(pixelArray[heightOffset+1][widthOffset] + pixelArray[heightOffset+1][widthOffset+1])/2;
	row[1][1] = (double complex)(pixelArray[heightOffset+1][widthOffset] - pixelArray[heightOffset+1][widthOffset+1])/2;

	spectre[0][0] = (double complex)(row[0][0] + row[1][0])/2;
	spectre[0][1] = (double complex)(row[0][1] + row[1][1])/2;
	spectre[1][0] = (double complex)(row[0][0] - row[1][0])/2;
	spectre[1][1] = (double complex)(row[0][1] - row[1][1])/2;

	return spectre;
}

double complex ** reverseButterfly(double complex ** quarter1, double complex ** quarter2, double complex ** quarter3, double complex ** quarter4, uint32_t height, uint32_t width){
	//memory
	double complex **leftVerticalMatrix = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		leftVerticalMatrix[i] = (double complex *) malloc( sizeof(double complex) * width/2);
	}

	double complex **rightVerticalMatrix = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		rightVerticalMatrix[i] = (double complex *) malloc( sizeof(double complex) * width);
	}

	double complex **result = (double complex**)malloc(sizeof(double complex *) * height);
	for(int i = 0; i < height; i++ ) {
		result[i] = (double complex *) malloc( sizeof(double complex) * width);
	}

//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width/2; i++){
			if (j < height/2){
				leftVerticalMatrix[j][i] = ( quarter1[j][i] + quarter3[j][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
			}
			else
				leftVerticalMatrix[j][i] = ( quarter1[j-height/2][i] + quarter3[j-height/2][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
		}
	}
//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width/2; i++){
			if (j < height/2){
				rightVerticalMatrix[j][i] = ( quarter2[j][i] + quarter4[j][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
			}
			else
				rightVerticalMatrix[j][i] = ( quarter2[j-height/2][i] + quarter4[j-height/2][i] * cexp( -I * 2 * PI * (i+j*width/2)  / (height*width/2) )  )/2;
		}
	}
//--------------------------------------------------------------------
	for(int j = 0; j < height; j++){
		for(int i = 0; i < width; i++){
			if (i < width/2){
				result[j][i] = ( leftVerticalMatrix[j][i] + rightVerticalMatrix[j][i] * cexp( -I * 2 * PI * (i+j*width)  / (height*width) )  )/2;
			}
			else
				result[j][i] = ( leftVerticalMatrix[j][i-width/2] + rightVerticalMatrix[j][i-width/2] * cexp( -I * 2 * PI * (i+j*width)  / (height*width) )  )/2;
		}
	}

	return result;
}

double complex ** reverseSpectre(double complex ** pixelArray, uint32_t widthSize, uint32_t heightSize, uint32_t widthOffset, uint32_t heightOffset){
	double complex ** quarter1;
	double complex ** quarter2;
	double complex ** quarter3;
	double complex ** quarter4;

	if (widthSize == 2 && heightSize == 2){
		//printf("Inside matrix\n");
		return (double complex **)reverseFastFurieTransform(pixelArray, widthOffset, heightOffset);
	}
	else{
		//������� �������� ������
		quarter1 = reverseSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset, heightOffset);
		quarter2 = reverseSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset + widthSize/2, heightOffset);

		//������ ��������
		quarter3 = reverseSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset, heightOffset + heightSize/2);
		quarter4 = reverseSpectre(pixelArray, widthSize/2, heightSize/2, widthOffset + widthSize/2, heightOffset + heightSize/2);
	}
	return (double complex **)reverseButterfly( quarter1, quarter2, quarter3, quarter4, heightSize, widthSize );
}
